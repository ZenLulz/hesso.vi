\documentclass[a4paper]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[margin=1.1in]{geometry}
\usepackage[english]{babel}
\usepackage{charter}
\usepackage{graphicx}
\usepackage{parskip}
\usepackage{csquotes}

\usepackage[style=numeric]{biblatex}
\addbibresource{vi.bib}

% Define colors for hyperlinks
\usepackage[dvipsnames]{xcolor}
\definecolor{theblue}{rgb}{0.02,0.04,0.48}

% Package to handle links (handle long url as well)
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref}
\hypersetup{
	linkcolor  = theblue,
	citecolor  = theblue,
	urlcolor   = theblue,
	colorlinks = true,
}

\usepackage{setspace} % increase interline spacing slightly
\setstretch{1.1}

\title{SwissQuake\\Information Visualization --- Practical work}
\author{Damien Rochat, Jämes Ménétrey and Michaël Sandoz\\Master of Science in Engineering\\University of Applied Sciences and Arts Western Switzerland\\{\texttt{firstname.lastname @ master.hes-so.ch}}}
\date{\today}

\begin{document}
	\maketitle

	\tableofcontents
	
	\section{Introduction}
	\subsection{Why this project ?}
	Switzerland suffers several earthquakes every month, but in the end, who really realizes it? This project aims to raise public awareness of their frequency, their location and finally to deduce the most seismic zones of the country. This information can be useful to certain types of buildings, such as deep geological repositories or data centers.\cite{Wikipedia2018Deep}


	\subsection{Where can I test ?}
	The application is hosted on the platform Heroku.\footnote{\url{https://swissquake.herokuapp.com/}} Please be aware that the free hosting plan is used and may take a couple of seconds to wake up for the first request. The source code is available in a public git repository located on Bitbucket.\footnote{\url{https://bitbucket.org/ZenLulz/hesso.vi/}}
	
	\section{Dataset}
	The dataset used for the visualization of earthquakes comes from the public Earthquake Catalog of Switzerland 2009.\cite{Service2009Earthquake} It integrates the following basic information:

	\begin{itemize}
		\item The Macroseismic Earthquake Catalog of Switzerland, with events from AD 250, revised and supplemented to 2008.
		\item Yearly reports of the Swiss earthquake commission since 1879.
		\item Earthquake locations of the instrumental networks of the SED since 1975.
		\item Supplemented by 12 earthquake catalogs from neighbouring and international agencies.
	\end{itemize}

	The data are viewable in a table on their website with a couple of data transformations, such as filtering. A visual representation is also available on a map of Switzerland.\cite{Service2009Visualisation} SwissQuake relies on the following set of information:

	\begin{itemize}
		\item The date to filter the data based on the occurrence,
		\item the latitude/longitude to localize the epicentre, and
		\item the moment magnitude scale (Mw), preferred to Richter scale.
	\end{itemize}
	


	\section{Conception}
	The first weeks of the project have been allocated to brainstorm on the capabilities of the program to create, the different visualizations to provide to the users and the ways of interaction. This section describes the decisions, the features and the representation modes we have agreed on.


	\subsection{Features}

	\paragraph{The Swiss map.} People who use map tools such as \emph{Google Maps} got used to a flat map on the page. Those tools tend to implement the same ways of interactions, for example the mouse scroll to zoom in/out, the drag and drop to move the map around and the click to gather detailed information of a specific location. We would like to keep those habits and propose a similar experience, so the users hopefully understand intuitively how to interact with our application. The earthquakes are then integrated onto a map component, similarly as a \emph{scatter plot} at high levels of zoom (i.e. close to the earth). After a couple of tests with examples of scatter plots integrated onto a map, we realized that the visualization of the information was very poor at low levels of zoom (i.e. far from the earth). We decided to deactivate the scatter plot when the level of zoom is reduced and activate a heat map, which gives a much better trend of data aggregation.

	\paragraph{The historic over time.} While the Swiss map represents a holistic view of the data on a geographical region, a \emph{histogram} is used to represent a holistic view of the magnitude of the earthquakes over time. This visualization mode enables the users to understand the power of the earthquakes over the years. We chose to connect the histogram with the viewport of the map, meaning that the histogram only displays the data related to the geographical area displayed on the map. The users are able to interact with the histogram and provides a tooltip with the selected year and the magnitude level in average at that moment.

	\paragraph{Predefined viewports.} In addition of enabling the users to navigate through the map, a couple of predefined viewports (predetermined locations on the map) have been introduced, highlighting interesting earthquake data. We hope this functionality motivates the users to play with the map with different levels of zoom.

	\paragraph{The filters.} Aside of the interaction modes offered to the users, two methods of filtering the data set are provided. Indeed, the number of earthquakes is very important and this is why we decided to allow the ability to remove undesired earthquakes based on their occurrence in time and by magnitude. Thanks to the filters, the users can have a holistic view of the biggest earthquakes that happened in the last 20 years, for example.


	\subsection{Interaction pipeline}

	The features of the software cover the four steps and three actions of the interaction pipeline, as presented during the course, detailed in the figure \ref{fig:interaction-pipeline}.\footnote{Source: 6\textsuperscript{th} lecture, \emph{Interaction}.} The step of \emph{data transformation} is the loading of the collection of earthquakes and the filtering of the data by the time and magnitude. The \emph{visual mapping} cannot be changed in the application, this is the map and the histogram. Finally, the \emph{view transformation} is provided by the map, when zooming in/out and moving around. The histogram is adapted based on the viewport of the map to describe the currently visualized data.

	\begin{figure}
		\centering
		\includegraphics[width=\columnwidth]{images/interaction-pipeline.png}
		\caption{The interaction pipeline.}
		\label{fig:interaction-pipeline}
	\end{figure}


	\subsection{Schneiderman's mantra}
	We aim to develop the software in regards to the mantra of Ben Schneiderman, written in 1996\cite{Shneiderman:1996:ETD:832277.834354}:

	\begin{quote}
		\textbf{Overview} first, \textbf{zoom} and \textbf{filter}, then \textbf{details-on-demand}.
	\end{quote}

	When arrived on the application, an \emph{overview} of the data is displayed on the map of the Switzerland and the histogram. The users have then the ability to \emph{zoom} in a specific location using the usual gestures with the map or using the viewports with predefined locations. They can also \emph{filter} the displayed earthquakes to eliminate the unwanted information and finally, they can \emph{gather details} on a specific earthquake by clicking on it.

	In addition of the mantra, the user interaction of the application has been designed so it any action performed by the users can always be undone easily (e.g. zoom in/out, change values of filters), enhancing the iterations of the Donald Norman's action cycle.\footnote{Source: 6\textsuperscript{th}, \emph{Interaction}.}\cite{Norman1989Design}


	\subsection{Prototyping}
	Based on the specifications detailed in the previous sections, it has been decided to draw a mockup beforehand, in order to align the team on a single, well-thought approach. The tool \emph{Balsamiq} has been used to create the mockup, illustrated in the figure \ref{fig:mockup}. The Web application is centered around the map component, as it is the main area where the users work. A sidebar is added in order to manage the filters and the histogram. Finally, the viewports are visible in a bottom bar.

	\begin{figure}
		\centering
		\includegraphics[width=\columnwidth]{images/mockup.png}
		\caption{The mockup used before writing the application.}
		\label{fig:mockup}
	\end{figure}



	\section{Information visualization concerns}
	This section is dedicated to the explanation of the choices we have made to design the interface and the interactions of the Web application.

	\subsection{Technologies}
	The Web application relies on two main technologies: \emph{Mapbox} to display the map of the world and \emph{Chart.js} to draw the histogram. We initially picked \emph{Google Maps}, but we reconsidered this choice when we realized that the heat map was not providing a good user experience while visualizing the earthquakes at very high/low level of zoom. Indeed, the heat map was saturating at low levels, while was not aggregating the data at high levels. The trade-off has been found with \emph{Mapbox}, which provides a way to have a scatter plot at high levels of zoom and a proper heat map at low levels of zoom. The performance of the latest is also better with a lot of data to render.

	Otherwise, the stack of back-end technologies is \emph{JavaScript} with \emph{jQuery} for the scripting language, \emph{jQuery UI} for the sliders of the filters, \emph{Bootstrap} for the design framework, \emph{postal.js} for the internal event bus, \emph{lodash} for applying the filters in a functional programming way over the data.


	\subsection{Colors set}
	The application is conservative with the use of colors. The map and the sidebar remain color-neutral. The colors are only used for highlighting information that the users are manipulating, such as the heat map and the histogram. Indeed, colors help the perception as a \emph{preattentive processing}, so the users are more focused on the transformation of the data and not distracted with the features of the application.\footnote{Source: 3\textsuperscript{rd} lecture, \emph{Human perception, mental models}.}

	The application also considers the users who are color blind. There are multiple types of colorblindness to evaluate while designing a software that relies on colors to display information.\footnote{Source: 4\textsuperscript{th} lecture, \emph{Information design and graphic semiology}.} To help understand whether the application can still be used by people that can partially see colors, we have made a screenshot of the SwissQuake application and uploaded it to the online tool \emph{Coblis}.\footnote{Color Blindness Simulator, available at \url{http://www.color-blindness.com/coblis-color-blindness-simulator/}} This simulator helped us to understand whether the heat map can be correctly understood when the colors set is altered. Fortunately, the color range of the heat map can be properly interpreted, even when the person is achromatopsia (cannot distinguish colors, only see in black, white and shades of grey). This is because the heat map uses \emph{distinct colors} as hues, which are red, yellow, green and blue.


	\subsection{Semiology}
	The semiology is the science of signs, symbols and their meaning. Thus, this field of study elaborates how humans intuitively perceive them, this means people don't realize they are evaluating symbols based on a set of characteristics, called \emph{visual features}.\footnote{Source: 4\textsuperscript{th} lecture, \emph{Information design and graphic semiology}.} This project capitalizes on semiology to provide additional information gracefully, without having to display numbers everywhere. This is the case for the scatter plot of earthquakes integrated to the map. Indeed, the earthquakes are represented by circles, which use two different visual features: the hue and the size. Those variables are proportional to the force of magnitude of the events. Thereby, people don't have to read a number attached to each earthquake to realize the power of those. A benefit of this strategy is that the users can quickly compare the earthquakes based on those two variables.


	\subsection{Data-ink ratio}
	While the histogram represents a small part of the user interface, we applied the best practices for it and this includes notably the data-ink ratio. The following steps have been taken to reduce the footprint of the ink if printed:

	\begin{itemize}
		\item remove the border of the chart (the lines between the numbers on the axes and the bars),
		\item remove the vertical grid lines, and
		\item reduce the horizontal grid lines ink-ratio by converting them to dashed lines. The library is not able to display the grid lines over the bars, so it's not possible to only represent the grid lines as white lines above the bars. We tried to remove entirely the horizontal grid lines, but the visualization of the magnitude was not good at all.
	\end{itemize}


	\subsection{User experience and usability}
	When working on the software, we focused to develop an intuitive interface, that users are going to enjoy. The notion of user experience is very important in the context of software development, because it increases the likelihood that the users have a good feeling while playing with it and feel positive emotions. Thus, the application relies on well-known gestures to operate, such as the scroll to zoom in/out of the map, a sidebar that can be retracted, well-understood components such as a double slider to manipulate the filters of the years and the magnitude, in order to smooth the experience as much as possible.
	
	The notion of UX must be distinguished from the notion of usability, which stands for the quality attribute of the user interface.\footnote{Source: 7\textsuperscript{th} lecture, \emph{GUI design}.} In order to minimize the number of steps/clicks to perform, the bottom bar containing the viewports has been removed and merged into the side bar. Thus, the features of the application are presented in a single unified location, enhancing its conciseness and provide a nice visual structure using white boxes. The users are invited to play with the different options (discoverability), while not being penalized by changing a setting, because it can be easily rolled back as the side bar is always present. Indeed, the reversibility of users' actions encourages the exploration. The website is a single-page application, where the most important features are available \emph{above-the-field}, meaning that it does not require the users to scroll down to access them. A personalized scroll bar has been added to the side bar to indicate that there is more content underneath anyway. Finally, the interface focuses on the users and their tasks, not on the underlying stack of technologies.


	\subsection{The tasks by data type taxonomy}
	The information exploration should be a pleasure, but it's sometimes hard to explore a huge database. That's what Ben Schneiderman said in his article \emph{The Eyes Have It: A Task by Data Type Taxonomy}.\cite{Shneiderman:1996:ETD:832277.834354} This publication creates a taxonomy of tasks and data types that are still actively used to design a good user interface. This section maps the features of the application to the concept developed by Ben Schneiderman.

	\subsubsection{The data types}
	
	The application mostly relies on a data type called \emph{2-dimensional}. This is the case of the map of the world. This component contains other \emph{2-dimensional} elements, such as a heat map and a scatter plot of the earthquakes. After consulting the other data types, it has been decided that the \emph{2-dimensional} representations are the best for the needs of this application. Introducing a \emph{3-dimensional} data type can quickly complicate the interaction with the application, such as a 3D map of the Switzerland, while not being needed to fulfil the goals of the users.

	\subsubsection{the tasks}

	\paragraph{Overview} It can be defined as a quick view of the entire collection of the earthquakes, with a movable field-of-view. Thanks to the map, the overview capability is certainly the central point of the application, and welcome the users with a heat map of all the systematically recorded earthquakes.

	\paragraph{Zoom} The field-of-view can be freely adjusted by the users, both on desktop (scroll and move) or on mobile (pinch and point and move).

	\paragraph{Filter} As the database is huge, the users would like to remove unwanted information. It can be done by years and magnitude using the ad-hoc sliders in the side bar.

	\paragraph{Details-on-demand} When the users are interested in a particular earthquake, all the information can be gathered about it by clicking on its event.

	\paragraph{History} The application does not have multiple states, meaning that the side bar with the settings are always under the eyes of the users. Therefore, if a parameter is modified, it can be reversed with ease.

	The application does not support the other tasks, such as \emph{Relate} and \emph{Extract}. This point is discussed in the section dedicated the potential improvements of the application.



	\section{Application insights}
	This section illustrates the Web application and its components once implemented.


	\subsection{Application dashboard}

	\begin{figure}
		\centering
		\includegraphics[width=\columnwidth]{images/screenshots/dashboard.png}
		\caption{The application when loaded.}
		\label{fig:demo-dashboard}
	\end{figure}

	When landing on the application, the map is automatically populated with the earthquakes and the level of zoom is set so the users can see an overview of the data across the country, as detailed in the figure \ref{fig:demo-dashboard}. The filters limit the scope of the data to represent the best possible view of the earthquakes. The users are then intuitively invited to manipulate the map, as this is the biggest part of the application.


	\subsection{The sidebar}

	\begin{figure}
		\centering
		\includegraphics[width=0.5\columnwidth]{images/screenshots/sidebar-translucent.png}
		\caption{The sidebar is translucent and can be collapsed.}
		\label{fig:demo-sidebar-translucent}
	\end{figure}

	The side bar contains the other components of the application. Its background is translucent, indicating to the users that it can be collapsed, using the ad-hoc arrow button, as shown in the figure \ref{fig:demo-sidebar-translucent}. The components are isolated by categories using white boxes. If the viewport of the browser does not have enough height to display the entire set of features, a scroll bar is automatically added, indicating to the users that they can scroll down to access more content.


	\subsubsection{The filters}

	\begin{figure}
		\centering
		\includegraphics[width=0.5\columnwidth]{images/screenshots/filters.png}
		\caption{The year filter displays a warning message before 1500.}
		\label{fig:demo-filters}
	\end{figure}

	The filters use a double slider, enabling the users to specify a range. It works like a regular slider, except the minimum point cannot go beyond the maximum and the reverse. When the state of the sliders is changed, the values of the range are updated above. Due to the nature of the earthquakes, the science could not collect such events systematically before 1500. To prevent the users thinking that there was a very poor number of earthquakes prior that year, a warning message is displayed when the slider select years before, as illustrated in the figure \ref{fig:demo-filters}.


	\subsubsection{The histogram}

	\begin{figure}
		\centering
		\includegraphics[width=0.5\columnwidth]{images/screenshots/histogram.png}
		\caption{The histogram while highlighting a specific year.}
		\label{fig:demo-histogram}
	\end{figure}

	The histogram displays the average of magnitude for the year in the range selected by the filter of time. When the time range is large ($> 250$) the years are grouped by 10, when the time range is medium ($> 50$), the years are grouped by 2 and finally when the range is small ($\leq 50$), the years are not grouped. The users are able to highlight a bar of the chart to receive more information, such as the exact number of the average of magnitude, as detailed in the figure \ref{fig:demo-histogram}. The content of the histogram automatically changes depending on the selected range in the filter of years.


	\subsubsection{The viewports}

	\begin{figure}
		\centering
		\includegraphics[width=0.5\columnwidth]{images/screenshots/viewports.png}
		\caption{The viewports offer predefined location on the map.}
		\label{fig:demo-viewports}
	\end{figure}

	The viewports are buttons that once pressed, modify the area covered by the map, bringing the users to the specified location. When a viewport is hovered by the cursor, its opacity is reduced, giving feedback to the users, as illustrated on the figure \ref{fig:demo-viewports}.


	\section{User testing}
	A phase of user testing has been conducted to verify the good experience of the user interface. This test includes three people with no prior knowledge of the application and with an average technical level. The application has been appreciated and the users feel the software is useful in case of building a house. In addition, they provided a couple of improvements, listed below.

	\paragraph{The change of the histogram was not understood} The histogram is updated based on the data in the viewport of the map. This means it only takes into account the earthquakes currently visualized on the screen. To overcome this issue, a label has been added above the histogram explaining its role and why it gets updated when the user navigate in the map.

	\paragraph{The selection of a bar in the histogram highlights the corresponding earthquakes} The users had some issues to map the data in the histogram and the corresponding earthquakes on the map. This would increase the link between the two representations of the same data source. The development team agree with this idea, but don't have the time to implement it, unfortunately.

	\paragraph{The bar in the histogram should match the color rule of the earthquake circles} As a recall, the circle color of the earthquakes varies from blue to red, proportionally to their magnitude. The idea is to apply the same logic to the bar chart, depending on the average value, in order to have a better visual mapping between the map and the histogram. This case has been reviewed with the development team and this feature has been added into the application.


	\section{Conclusion}
	The project is fully functional, reactive and provides a great insight of the earthquakes in Switzerland. In addition, it enabled us to anchor some concepts covered during the lectures of the course, such as the semiology and the interaction pipeline by planning the application with these concepts in mind before writing the application. It took a bit of a time to properly analyse the ways of rendering the information on the map but once decided, the Web application has been implemented fairly quickly.

	\subsection{Improvements}
	The improvements below have been imagined by the development team and does not repeat the ideas expressed by the user testing phase.

	\paragraph{The missing tasks} While analysing the Schneiderman's tasks, the application is missing the abilities to \emph{relate} and \emph{extract} the information. After an additional brainstorming session, some propositions have been made to fulfil these two missing points. The \emph{relate} operation can be added when hovering a given earthquake with the cursor. The other similar earthquakes can be highlighted on the map with an animation, for example. The \emph{extract} information can be an export in JSON of the currently displayed earthquakes and the ability to export the current viewport of the map into an image that can be downloaded.

	\paragraph{Pane resizing} In addition of the existing feature to hide the side bar, an option to resize it can be added for the users with a smaller screen, such as a tablet or a mobile.

	\paragraph{More precision with the slider of years} The range of years is pretty large, from 250 to 2008. While the double slider component is easy to use, it is sometimes hard to select a year with precision. To solve this issue, a \emph{Fisheye view} can be introduced on the double slider when the users start to slide slowly to gain in accuracy.


	\printbibliography
	
\end{document}
