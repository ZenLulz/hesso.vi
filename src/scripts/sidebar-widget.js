_.mixin({
    inRangeInclusive: function (number, start, end) {
        return (_.inRange(number, start, end) || number == end);
    },
    floorToMultipleOf: function (number, multiple) {
        return number - (number % multiple);
    }
});

$(function() {

    var SidebarWidget = (function () {
        var self = this;
        self.$container = undefined;
        
        var MwChart;
        var MwChartAggregations = {
            y10: 1, // by 10 years
            y2:  2, // by 2 years
            y1:  3, // by 1 year
            m1:  4, // by 1 month
            all: 5, // no aggregation
        };
        var MwChartCurrentAggregation;

        // Constructor
        self.init = function (containerId) {
            self.$container = $('#' + containerId);

            generateChart();
            initToggle();
            bindViewportsClick();

            window.earthquakes.ready(function () {
                initTimeSlider();
                initMagnitudeSlider();
                updateMwChart();

                window.channel.subscribe('earthquakes.updated', updateMwChart);
            });
        };

        var generateChart = function() {
            var chart = self.$container.find('.magnitudeChart')[0];
            chart.height *= 1.4;
            MwChart = new Chart(chart.getContext('2d'), {
                type: 'bar',
                data: {
                    datasets: [{
						backgroundColor: 'rgb(54, 162, 235)',
						borderColor: 'rgb(54, 120, 235)',
						data: [],
                        pointRadius: 1,
                        fill: false,
                        lineTension: 0,
                    }]
                },
                options: {
                    responsive: true,
                    showLines: false,
                    title: {
                        display: false,
                    },
                    tooltips: {
                        enabled: true,
                        callbacks: {
                            title: function (tooltipItem, dataset) {
                                var date = tooltipItem[0]['xLabel'];

                                if (MwChartCurrentAggregation == MwChartAggregations.y10) {
                                    return date+"-"+(parseInt(date)+9);
                                }
                                else if (MwChartCurrentAggregation == MwChartAggregations.y2) {
                                    return date+"-"+(parseInt(date)+1);
                                }
                                else if (MwChartCurrentAggregation == MwChartAggregations.y1) {
                                    return date;
                                }
                                else if (MwChartCurrentAggregation == MwChartAggregations.m1) {
                                    return date;
                                }
                            },
                            label: function(tooltipItem, dataset) {
                                return " Mw: "+tooltipItem['yLabel']+" (avg)";
                            }
                        },
                        backgroundColor: '#ffffff',
                        cornerRadius: 3,
                        borderWidth: 1,
                        borderColor: 'rgba(0, 0, 0, 0.1)',
                        titleFontColor: '#212529',
                        bodyFontColor: '#212529',
                    },
                    hover: {
                        mode: null
                    },
                    legend: false,
                    scales: {
                        xAxes: [{
                            type: 'time',
                            distribution: 'linear',
                            time: {
                                parser: 'YYYY',
                                unit: 'year',
                                // tooltipFormat: 'YYYY',
                            },
                            bounds: 'ticks',
                            scaleLabel: {
                                display: true,
                                labelString: 'Time [year]'
                            },
                            gridLines: {
                                display: false,
                                drawBorder: false
                            }
                        }],
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: 'Magnitude [Mw]'
                            },
                            gridLines: {
                                display: true,
                                drawBorder: false,
                                borderDash: [5, 8],
                                borderDashOffset: 5
                            },
                            ticks: { // default scale
                                min: 0,
                                max: 8,
                                beginAtZero: true,
                            },
                        }]
                    }
                }
            });
        };

        var updateMwChart = function () {
            var filters = window.earthquakes.getFilters();

            var years_range = filters.year.max - filters.year.min;
            if      (years_range > 250) MwChartCurrentAggregation = MwChartAggregations.y10;
            else if (years_range > 50)  MwChartCurrentAggregation = MwChartAggregations.y2;
            else if (years_range > 0)  MwChartCurrentAggregation = MwChartAggregations.y1;
            // else if (years_range > 20)  MwChartCurrentAggregation = MwChartAggregations.y1;
            // else if (years_range > 0)   MwChartCurrentAggregation = MwChartAggregations.m1;
            else                        MwChartCurrentAggregation = MwChartAggregations.y10;
            
            var data = _
                .chain(window.earthquakes.getData().features)

                // filter dataset, depending years and magnitude
                .filter(function (o) {
                    return _.inRangeInclusive(o.properties.year, filters.year.min, filters.year.max)
                        && _.inRangeInclusive(o.properties.Mw, filters.Mw.min, filters.Mw.max)
                        
                        // Disclaimer, geodesie not handled and will not works all over the world!
                        // But much more simple...
                        && _.inRangeInclusive(o.geometry.coordinates[0], filters.viewport.sw.lng, filters.viewport.ne.lng)
                        && _.inRangeInclusive(o.geometry.coordinates[1], filters.viewport.sw.lat, filters.viewport.ne.lat)
                })

                // convert item to (date, magnitude) paire
                .map(function (o) {
                    var x;

                    // group by 10 years
                    if (MwChartCurrentAggregation == MwChartAggregations.y10) {
                        x = _.floorToMultipleOf(o.properties['year'], 10).toString();
                    }

                    // group by 2 years
                    else if (MwChartCurrentAggregation == MwChartAggregations.y2) {
                        x = _.floorToMultipleOf(o.properties['year'], 2).toString();
                    }

                    // group by year
                    else if (MwChartCurrentAggregation == MwChartAggregations.y1) {
                        x = o.properties['year'].toString();
                    }

                    // group by month
                    // the graphjs chart has trouble to handle month with a bar chart
                    // else if (MwChartCurrentAggregation == MwChartAggregations.m1) {
                    //     var date = o.properties['date/time'].split(' ')[0].split('/'); // drop time and split date
                    //     x = date[0] + '-' + date[1]; // keeps year and month
                    // }
                    
                    return {
                        x: x,
                        y: o.properties['Mw']
                    };
                })

                // aggregate by group
                .groupBy('x')
                .map(function (o) {

                    // take max magnitude
                    return _.maxBy(o, 'y');

                    // take avg magnitude
                    // return _.meanBy(o, 'y');
                })

                .value();

            // console.log(years_range);
            // console.log(data);
            
            // if (MwChartCurrentAggregation == MwChartAggregations.m1) {
            //     MwChart.options.scales.xAxes[0].time.parser = 'YYYY-MM';
            // }
            // else {
            //     MwChart.options.scales.xAxes[0].time.parser = 'YYYY';
            // }

            MwChart.data.labels = _.map(data, function (o) { return o.x; });
            MwChart.data.datasets[0].data = _.map(data).map(function (o) { return o.y; });
            MwChart.data.datasets[0].backgroundColor = _.map(data, function (o) {
                // Affine transformation for the range [2; 6]
                return getGradientColor('#36a2eb', '#ee3b2d', Math.min(1, Math.max(0, o.y - 2) / 4));
            });
            MwChart.update();
        };

        var initMagnitudeSlider = function () {
            var bounds = window.earthquakes.getBounds().Mw;
            var filters = window.earthquakes.getFilters().Mw;

            updateMagnitudeLabels(filters.min, filters.max);
            self.$container.find('.magnitude').slider({
                range: true,
                min: bounds.min,
                max: bounds.max,
                step: 0.1,
                values: [filters.min, filters.max],
                slide: function (event, ui) {
                    updateMagnitudeLabels(ui.values[0], ui.values[1]);
                },
                change: function (event, ui) {
                    window.channel.publish('earthquakes.filtered', {
                        magnitude: { min: ui.values[0], max: ui.values[1] }
                    });
                }
            });
        }

        var updateMagnitudeLabels = function (min, max) {
            self.$container.find('.magnitude-from').text(min);
            self.$container.find('.magnitude-to').text(max);
        };

        var initTimeSlider = function() {
            var bounds = window.earthquakes.getBounds().year;
            var filters = window.earthquakes.getFilters().year;

            updateTimeLabels(filters.min, filters.max);
            self.$container.find('.time').slider({
                range: true,
                min: bounds.min,
                max: bounds.max,
                values: [filters.min, filters.max],
                slide: function (event, ui) {
                    updateTimeLabels(ui.values[0], ui.values[1]);
                    displayYearWarningIfNeeded(ui.values[0], ui.values[1]);
                },
                change: function (event, ui) {
                    window.channel.publish('earthquakes.filtered', {
                        years: { min: ui.values[0], max: ui.values[1] }
                    });
                }
            });
        };

        var updateTimeLabels = function (min, max) {
            self.$container.find('.time-from').text(min);
            self.$container.find('.time-to').text(max);
        };
        
        var initToggle = function () {
            self.$container.find("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        };

        var bindViewportsClick = function() {
            self.$container.find(".viewport").click(function() {
                window.channel.publish("map.flyTo", {
                    lng: $(this).data("lng"),
                    lat: $(this).data("lat"),
                    zoom: $(this).data("zoom")
                });
            });
        }

        var displayYearWarningIfNeeded = function(minYear, maxYear) {
            var threshold = 1500;
            var $warning = self.$container.find('.warning-year');

            if(minYear < threshold || maxYear < threshold) {
                $warning.show();
            } else {
                $warning.hide();
            }
        }

        var getGradientColor = function(start_color, end_color, percent) {
            // strip the leading # if it's there
            start_color = start_color.replace(/^\s*#|\s*$/g, '');
            end_color = end_color.replace(/^\s*#|\s*$/g, '');
         
            // convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
            if(start_color.length == 3){
              start_color = start_color.replace(/(.)/g, '$1$1');
            }
         
            if(end_color.length == 3){
              end_color = end_color.replace(/(.)/g, '$1$1');
            }
         
            // get colors
            var start_red = parseInt(start_color.substr(0, 2), 16),
                start_green = parseInt(start_color.substr(2, 2), 16),
                start_blue = parseInt(start_color.substr(4, 2), 16);
         
            var end_red = parseInt(end_color.substr(0, 2), 16),
                end_green = parseInt(end_color.substr(2, 2), 16),
                end_blue = parseInt(end_color.substr(4, 2), 16);
         
            // calculate new color
            var diff_red = end_red - start_red;
            var diff_green = end_green - start_green;
            var diff_blue = end_blue - start_blue;
         
            diff_red = ( (diff_red * percent) + start_red ).toString(16).split('.')[0];
            diff_green = ( (diff_green * percent) + start_green ).toString(16).split('.')[0];
            diff_blue = ( (diff_blue * percent) + start_blue ).toString(16).split('.')[0];
         
            // ensure 2 digits by color
            if( diff_red.length == 1 ) diff_red = '0' + diff_red
            if( diff_green.length == 1 ) diff_green = '0' + diff_green
            if( diff_blue.length == 1 ) diff_blue = '0' + diff_blue
         
            return '#' + diff_red + diff_green + diff_blue;
          };

        return self;
    })();

    SidebarWidget.init('sidebar-wrapper');
});
