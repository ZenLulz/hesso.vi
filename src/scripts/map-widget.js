$(function() {

    var MapWidget = (function () {
        var self = this;
        self.$container = undefined;
		        
        self.init = function (containerId) {        
            self.$container = $('#' + containerId);

            self.map = new mapboxgl.Map({
                container: containerId,
                style: 'mapbox://styles/damienrochat/cjohhv14w0cyk2rqn97behtf5',
            });

            // fit to switzerland bounds
            self.map.fitBounds([[5.8638, 45.7784], [10.5160, 47.8454]], {
                padding: 30,
				zoom: 0,
            });
            
            // prevent user to scroll out Swiss territory
            var sw = new mapboxgl.LngLat(3.4938, 44.4284);
            var ne = new mapboxgl.LngLat(14.5160, 49.3454);
            self.map.setMaxBounds(new mapboxgl.LngLatBounds(sw, ne));

            self.map.on('load', function () {
                window.earthquakes.ready(function () {			
					displayEarthquakes();
					
					window.channel.subscribe('earthquakes.updated', updateFilters);

					window.channel.subscribe('map.flyTo', function(data) {
						flyTo(data.lng, data.lat, data.zoom);
					});		
				});
			});

			map.on('click', 'earthquakes-points', function (e) {
				var coordinates = e.features[0].geometry.coordinates.slice();
				var props = e.features[0].properties;

				var html = '<div class="earthquake-popup">';
				if (props['date/time'].length > 0) html += '<p><strong>Date/Time:</strong> '+props['date/time']+'</p>';
				if (props['epi_area'].length > 0) html += '<p><strong>Area:</strong> '+props['epi_area']+'</p>';
				if (props['Mw'] !== null) html += '<p><strong>Mw:</strong> '+props['Mw']+'</p>';
				if (props['epi_intensity'].length > 0) html += '<p><strong>Epicenter intensity:</strong> '+props['epi_intensity']+'</p>';
				if (props['max_intensity'].length > 0) html += '<p><strong>Max intensity:</strong> '+props['max_intensity']+'</p>';
				html += '</div>';

				new mapboxgl.Popup()
					.setLngLat(coordinates)
					.setHTML(html)
					.addTo(map);
			});

			// Change the cursor to a pointer when the mouse is over the places layer.
			map.on('mouseenter', 'earthquakes-points', function () {
				map.getCanvas().style.cursor = 'pointer';
			});

			// Change it back to a pointer when it leaves.
			map.on('mouseleave', 'earthquakes-points', function () {
				map.getCanvas().style.cursor = '';
			});
			
			map.on('moveend', function() {
				var bounds = self.map.getBounds();
				window.channel.publish('earthquakes.filtered', {
					viewport: {
						ne: { lng: bounds._ne.lng, lat: bounds._ne.lat },
						sw: { lng: bounds._sw.lng, lat: bounds._sw.lat },
					}
				});
			});
		};
		
		var updateFilters = function () {
			var filters = window.earthquakes.getFilters();
			_.forEach(['earthquakes-points', 'earthquakes-heatmap'], function(layer) {
				map.setFilter(layer, [
					'all',
					['>=', 'year', filters.year.min],
					['<=', 'year', filters.year.max],
					['>=', 'Mw', filters.Mw.min],
					['<=', 'Mw', filters.Mw.max]
				]);
			});
		}

        var flyTo = function(lng, lat, zoom) {
            self.map.flyTo({
				center: [lng, lat],
				zoom: zoom
			});
        }

		const HEATMAP_THRESHOLD = 12; // 0: whole world, // 16: most detailed
		const FADE_INTERVAL = 2;
        var displayEarthquakes = function () {
			// inspired by https://www.mapbox.com/mapbox-gl-js/example/heatmap-layer/

			map.addSource('earthquakes', {
				'type': 'geojson',
				'data': window.earthquakes.getData()
			});
			
			map.addLayer({
				'id': 'earthquakes-heatmap',
				'type': 'heatmap',
                'source': 'earthquakes',
				'maxzoom': HEATMAP_THRESHOLD,
				'paint': {

					// earthquakes weight, depending magnitude
					'heatmap-weight': [
						'interpolate',
						['linear'],
						['get', 'Mw'],
						0, 0,
						7, 1
					],

					'heatmap-intensity': [
						'interpolate',
						['linear'],
						['zoom'],
						0, 1,
						HEATMAP_THRESHOLD, 3
					],
					
					'heatmap-radius': [
						'interpolate',
						['linear'],
						['zoom'],
						0, 0,
						HEATMAP_THRESHOLD, 10
					],

					// use default colors
					// 'heatmap-color': [
					// 	'interpolate',
					// 	['linear'],
					// 	['heatmap-density'],
					// 	0, 'rgba(252, 146, 114, 0)',
					// 	0.2, '#ee3b2d',
					// 	0.4, '#cb181d',
					// 	0.6, '#a50f15',
					// 	0.8, '#67000c',
					// 	1, '#4e0009'
					// ],		

					// fade-out heatmap opacity
					'heatmap-opacity': [
						'interpolate',
						['linear'],
						['zoom'],
						HEATMAP_THRESHOLD-FADE_INTERVAL, 0.6,
						HEATMAP_THRESHOLD, 0
					]
				}
			});

            map.addLayer({
                'id': 'earthquakes-points',
                'type': 'circle',
				'source': 'earthquakes',
				'minzoom': HEATMAP_THRESHOLD-FADE_INTERVAL,
				'paint': {

					// circle size, depending magnitude
					'circle-radius': [
						'interpolate',
						['linear'],
						['get', 'Mw'],
						0, 0,
						7, 14
					],

					// circle color, depending magnitude
					// 'circle-color': [
					// 	'interpolate',
					// 	['linear'],
					// 	['get', 'Mw'],
					// 	0, '#fc9272',
					// 	2, '#fb6a4a',
					// 	3, '#ee3b2d',
					// 	4, '#cb181d',
					// 	5, '#a50f15',
					// 	6, '#67000c',
					// 	7, '#4e0009'
					// ],

					'circle-color': [
						'interpolate',
						['linear'],
						['get', 'Mw'],
						2, '#36a2eb',
						4, '#ee3b2d'
					],

					// fade-out circles opacity
					'circle-opacity': [
						'interpolate',
						['linear'],
						['zoom'],
						HEATMAP_THRESHOLD-FADE_INTERVAL, 0,
						HEATMAP_THRESHOLD, 0.8
					],

					// circles stroke style
					'circle-stroke-color': 'white',
					'circle-stroke-width': 1,

					// fade-out circles opacity
					'circle-stroke-opacity': [
						'interpolate',
						['linear'],
						['zoom'],
						HEATMAP_THRESHOLD-FADE_INTERVAL, 0,
						HEATMAP_THRESHOLD, 0.8
					]
				}
			});
		};

        return self;
    })();

    MapWidget.init('map-widget');
});
