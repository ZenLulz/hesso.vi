# SwissQuake

## Membres

 - Ménétrey Jämes
 - Rochat Damien
 - Sandoz Michaël

## Descriptif du projet

La Suisse subît plusieurs tremblements de terre par mois, mais au final, qui s'en rend réellement compte ? Cette application a pour but de sensibiliser le public a leur fréquence et également à leur localisation.

## Lien du projet

- https://bitbucket.org/ZenLulz/hesso.vi/
- https://swissquake.herokuapp.com/

## Plus d'informations

Le reste des informations est fourni dans la documentation du projet.